FROM java:8-jre
MAINTAINER Jordan NOURRY <j.nourry@werevo.com>

ADD ./target/auth-service-1.0.0.jar /app/
CMD ["java", "-Xmx200m", "-jar", "/app/auth-service-1.0.0.jar"]

EXPOSE 5000