package com.werevo.auth.service;

import com.google.common.io.Files;
import com.werevo.auth.client.MailServiceClient;
import com.werevo.auth.domain.User;
import com.werevo.auth.dto.ResetPasswordDTO;
import com.werevo.auth.dto.TokenValidationDTO;
import com.werevo.auth.repository.UserRepository;
import com.werevo.auth.utils.PasswordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Objects;

@Service
public class UserServiceImpl implements UserService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private static final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
	
	private static final ShaPasswordEncoder validatioTokenEncoder = new ShaPasswordEncoder();

	@Autowired
	private UserRepository repository;
	
	@Autowired
	private MailServiceClient mailServiceClient;

	@Value("${after.email.validation}")
	private String afterEmailValidation;

	@Override
	public Object create(User user) {
		
		User existing = repository.findByUsername(user.getUsername());
		Assert.isNull(existing, "user already exists: " + user.getUsername());

		String hash = encoder.encode(user.getPassword());
		user.setPassword(hash);
		
		String validationToken = validatioTokenEncoder.encodePassword(System.currentTimeMillis()+user.getUsername(), "validationTokenSalt");
		user.setValidationToken(validationToken);

		User saveUser = repository.save(user);
		TokenValidationDTO tokenValidationDTO = new TokenValidationDTO();
		tokenValidationDTO.setUsername(saveUser.getUsername());
		tokenValidationDTO.setToken(validationToken);
		mailServiceClient.sendMailToTokenValidation(tokenValidationDTO);
		log.info("new user has been created: %1, with authorization: %2", user.getUsername(), user.getAuthorities());
		return saveUser;
	}
	
	@Override
	public Object getOrCreate(User user) {
		
		User existing = repository.findByUsername(user.getUsername());
		return Objects.nonNull(existing)?existing: create(user);
	}
	
	@Override
	public void delete(String username) {
		
		User existing = repository.findByUsername(username);
		Assert.notNull(existing, "user don't exists: " + username);
		
		repository.delete(existing);

		log.info("user has been deleted: {1}", username);
	}

	@Override
	public Object getUserAuthority(String name) {
		
		User currentUser = repository.findByUsername(name);
		Assert.notNull(currentUser, "user don't exists: " + currentUser);
		
		return currentUser.getAuthorities().get(0);
	}

	@Override
	public void resetPassword(String username) {
		
		User existing = repository.findByUsername(username);
		Assert.notNull(existing, "user don't exists: " + username);
		String newPassword = PasswordUtils.randomAlphaNumeric(12);
		String hash = encoder.encode(newPassword);
		existing.setPassword(hash);
		repository.save(existing);
		log.info("password has been changed: {1}", username);
		ResetPasswordDTO resetPasswordDTO = new ResetPasswordDTO();
		resetPasswordDTO.setUsername(username);
		resetPasswordDTO.setNewPassword(newPassword);
		mailServiceClient.sendMailToResetPassword(resetPasswordDTO);
		log.info("sendMailToResetPassword from mail service has been invoked for : {1}", username);
		
	}
	
	@Override
	public String validateUser(String validationToken) throws IOException {
		
		User existing = repository.findByValidationToken(validationToken);
		Assert.notNull(existing, "user with validationToken : "+validationToken+" don't exists.");
		existing.setEnabled(true);
		repository.save(existing);
		log.info("Enabling user : {1}", existing.getUsername());

		return Files.toString(new File(afterEmailValidation), Charset.defaultCharset());
	}
}
