package com.werevo.auth.service;

import com.werevo.auth.domain.User;

import java.io.IOException;

public interface UserService {

	Object create(User user);
	
	Object getOrCreate(User user);
	
	void delete(String username);
	
	Object getUserAuthority(String name);
	
	void resetPassword(String username);
	
	String validateUser(String validationToken) throws IOException;
}
