package com.werevo.auth.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.werevo.auth.dto.ResetPasswordDTO;
import com.werevo.auth.dto.TokenValidationDTO;


/**
 * Created by Amine on 05/12/17.
 */
@FeignClient("mail-service")
public interface MailServiceClient {

	@RequestMapping(value = "/mail/resetpassword", method = RequestMethod.POST) 
	void sendMailToResetPassword(ResetPasswordDTO resetPasswordDTO);
	
	@RequestMapping(value = "/mail/sendvalidationtoken", method = RequestMethod.POST) 
	void sendMailToTokenValidation(TokenValidationDTO tokenValidationDTO);
	
	
}
