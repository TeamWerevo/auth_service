package com.werevo.auth.controller;

import com.google.common.io.Files;
import com.werevo.auth.domain.User;
import com.werevo.auth.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.Principal;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserService userService;
	
	// SERVICES DISPONIBLE DEPUIS LE FRONT
	@RequestMapping(value = "/authority", method = RequestMethod.GET)
	public Object getCurrentUserAuthority(Principal principal){
		return userService.getUserAuthority(principal.getName());
	}

	@RequestMapping(value = "/resetpassword", method = RequestMethod.POST)
	public void resetPassword(@Valid @RequestBody String username){
		userService.resetPassword(username);
	}

    @ResponseBody
    @RequestMapping(value = "/confirm", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public String confirmValidationToken(@RequestParam(value="validationToken") String validationToken) throws IOException {
		return userService.validateUser(validationToken);
    }
	
	// SERVICES DISPONIBLE UNIQUEMENT DEPUIS LE SERVEUR
	@RequestMapping(value = "/current", method = RequestMethod.GET)
	public Object getUser(Principal principal) {
		return principal;
	}

	@PreAuthorize("#oauth2.hasScope('server')")
	@RequestMapping(method = RequestMethod.POST)
	public Object createUser(@Valid @RequestBody User user) {
		return userService.create(user);
	}
	
	@PreAuthorize("#oauth2.hasScope('server')")
	@RequestMapping(value = "/getOrCreate", method = RequestMethod.POST)
	public Object getOrCreateUser(@Valid @RequestBody User user) {
		return userService.getOrCreate(user);
	}
	
	@PreAuthorize("#oauth2.hasScope('server')")
	@RequestMapping(value = "/delete", method = RequestMethod.POST)
	public void deleteUser(@Valid @RequestBody String username) {
		userService.delete(username);
	}
}
