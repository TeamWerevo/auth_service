package com.werevo.auth.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.security.core.GrantedAuthority;

@Entity
@Table(name="authorities")
public class Authority implements GrantedAuthority {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String authority;
	
	public Authority(){}
	
	public Authority(String role){
		this.authority = role;
	}
	
	public String getAuthority() {
	    return authority;
	}
	
	public void setAuthority(String authority) {
	    this.authority = authority;
	}
}