package com.werevo.auth.service;

import com.google.common.io.Files;
import org.junit.Test;

import java.io.File;
import java.nio.charset.Charset;

public class UserServiceImplTest {

    @Test
    public void should_retrieve_html_file_into_ressources_folder() throws Exception {
        File file = new File("B:/DEV/projects/auth_service/src/main/resources/afterEmailValidation.html");
        System.out.println(Files.toString(file, Charset.defaultCharset()));
    }
}